# Download 2018 Hackathon

Questo repository contiene informazioni e documentazione relativa all'Hackathon organizzata durante l'evento [Download 2018](https://download-event.io) svoltosi a Bergamo nelle giornate di Sabato 8 e Domenica 9 Settembre 2018.

# Collegamenti

- [Sito ufficiale dell'evento](https://download-event.io)
- [Regolamento generale hackathon](https://download-event.io/wp-content/uploads/2018/07/Regolamento_Generale_Hackathon_Download_Innovation_2018.pdf)
- [Descrizione del challenge](challenge-it.md) - [English Version](challenge-en.md)


# Partecipanti

## Empanadas

Gioele Coppolino, Beatrice Bramati, Ignazio Pandes, Matteo Rossi

[Proposta presentata](https://gitlab.com/download2018/team-empanadas)


## Fix.it

Sinéad Ward, Vincent Kaufmann

[Proposta presentata](https://gitlab.com/download2018/team-fix-it)


## Freedom surf club

Francesco Bognini, Diego Intra, Vladislav Malashevskyy, Francesco Pastore, Singh Gopal

[Proposta presentata](https://gitlab.com/download2018/team-freedom-surf-club)


## HTTP 451

Paolo Massenzana, Manuel Minuti, Lucas Galli, Marco Carrara, Oltion Abazi

[Proposta presentata](https://gitlab.com/download2018/team-HTTP451)


## Le paste termiche

Matteo Soldini, Cristian Livella, Davide Ghisleni, Gabriele Previtali, Simone Spreafico

[Proposta presentata](https://gitlab.com/download2018/team-le-paste-termiche)


## Le sbarre

Fabio Cavaleri, Aaron Tognoli, Domenico Gaeni, Luca Maccarini, Matteo Ceribelli

[Proposta presentata](https://gitlab.com/download2018/team-le-sbarre)


## Pota team

Valerio Bonacina, Michele Pugno, Emanuele Sacco, Edoardo De Cal, Cesare De Cal

[Proposta presentata](https://gitlab.com/download2018/team-pota)


## Undefined

Lorenzo Bianchi, Giovanni Fumagalli, Mauro Bianchi, Shima Fahima

[Proposta presentata](https://gitlab.com/download2018/team-undefined)



# Risultato

TBD
