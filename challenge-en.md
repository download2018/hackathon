# Download 2018 - Hackathon Challenge

## Introduction

This document is addressed to the people taking part in the hackathon organized during the event [Download 2018](https://download-event.io) and contains an introduction to and information about the proposed challenges.

## General information

In the spirit of the event, namely

> 2 days of knowledge, learning, sharing, projects, inspirations, creativity and **Innovation**

the solution to the proposed challenges can be presented in a number of ways: ideas, architectural proposals, algorithms, mockups, prototypes and all that can stimulate the imagination and encourage the passionate participation of hackers from all over the world.

For this reason we have chosen to:

- Avoid imposing technological restrictions: use the databases, tools, languages, frameworks and platforms you like best! (In accordance with the applicable [Release License](https://opensource.org/licenses))
- Limit the details of technical requirements for possible integrations with existing platforms: wherever possible we indicate in the challenges the presence of existing systems with which it would be useful or interesting to interact.
- Present not one but several challenges of a different nature. You can focus on a particular aspect of the proposed problems or try to tackle them all.

----

# The context

The reference theme is the sending of **reports of problems to the Municipality** by citizens. We can summarize a generic reporting system within the following steps:

1. The citizen fills in the data relating to his or her own profile.
2. The citizen selects the category of the report from a list (e.g.: Public lighting, Aqueduct, Road signs, etc.).
3. The citizen enters a description of the report: description of the problem, area/street, any images.
4. The relevant office receives and processes the report.
5. The citizen receives feedback on the status of the report: handling, comments, expected time of resolution, confirmation of resolution.

----

# Challenges

Thanks to the experience gained, the Municipality of Bergamo has helped us to formulate the following challenges:

## Challenge 1: Elimination of duplicates

In order to ensure a more efficient management, it is essential to reduce the number of duplicate reports: in case of a streetlight malfunctioning, it is likely that more than one citizen will send a request. How can we identify duplicate reports? Can we show the citizen some suggestions *("Did you mean...?")* even before he or she has completed the request insertion phase?

*We would like to suggest the presence of the [Bergamo WiFi](http://wifi.comune.bergamo.it) service as one of the potential sources of meta-information on the reporting.*

## Challenge 2: Routing to the competent office

How can we identify the appropriate reporting office as precisely as possible? What rules would you implement if you had a database with the history of the reports and any corrections (forwarding to other offices)?
How reliable is the categorization carried out by the citizen? Are there categories more prone to misaddressing?

*Please note: the database will not actually be provided during the competition.*

## Challenge 3: Feedback to the citizen

Once the report has been opened, how can the citizen be informed that the request has been taken care of? What do you suggest in order to keep the communication "alive"? How can the citizen monitor the status of the submitted requests? Would a rating system be useful as a way to highlight the most popular reports?

*We signal the possibility of integration with the [GIS platform](https://territorio.comune.bergamo.it/gfmaplet/?map=Orologis&initialExtent=551629;5060087;552645;5060586;32632&htmlstyle=combg) of the Municipality and the [IO Italia](https://io.italia.it/) ([GitHub](https://github.com/teamdigitale/italia-app) project).*

## Challenge 4: Security

What measures would you take to protect the system from cyber abuse and attacks? Is there a way to make the service usable by citizens, possibly allowing anonymous reports but without making the procedure of opening a report too onerous? Can we think of making a better CAPTCHA than the most popular ones or of using alternative approaches?

## Challenge 5: Information availability

The data contained in a reporting system may be of interest to several other systems. What types of API would you implement to make its consultation as easy, complete and flexible as possible for fellow developers?

*Usage example: the citizen municipal desk service could be extended to collect reports and their resolution status.*

## Challenge 6: Visualization & Gamification

If you wanted to see the current status of reports (closed, open, in process) in one or more dashboards, what data and in what format would you suggest to represent them?
Which indicators and functionalities would encourage a healthy competition between the Offices and improve their efficiency?

----

# Additional notes

## The evaluation

The project submitted by each team will be evaluated according to the criteria of:
* Usefulness / Value for the client
* Relevance to the theme and challenges presented
* Completeness: we will reward the teams that have been able to bring the project to the most advanced stage of elaboration / implementation; attention to functional technical requirements (such as ease of release, any documentation produced, etc.) will also be considered.
* Design / UX, user interface, usability, appeal
* Creativity / Innovation especially in approaching the challenges

## Publication of projects

The projects published on the [official gitlab project](https://gitlab.com/download2018/) of the event will be evaluated within the set deadlines. During the hackathon, the organizers will proceed to create a dedicated repository for each team and to ensure access to members (it is required to create an account on gitlab.com).
You can use other platforms to share code and make a final push on your team's repository.

----

Have fun,  
*The Hackathon owners*
